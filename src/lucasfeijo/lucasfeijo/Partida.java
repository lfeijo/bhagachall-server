package lucasfeijo;

/**
 * Created by lucasfeijo
 */
public class Partida {

    public interface Manager {
        void partidaEncerrou(Partida partida);
    }
    private Manager manager;

    public static final int JOGADOR_CABRAS = 1;
    public static final int JOGADOR_TIGRES = 2;
    public static final int MAX_CABRAS = 20;

    private char grade[][] = new char[5][5];
    private int jogadores[] = new int[2];
    private int vez = 0;
    private int cabras = 0;

    public Partida(Manager manager) {
        for (int i=0; i<5; i++) {
            for (int j=0; j<5; j++) {
                grade[i][j] = ' ';
            }
        }
        grade[0][0] = '1';
        grade[0][4] = '2';
        grade[4][0] = '3';
        grade[4][4] = '4';
        //jogadores[0] == JOGADOR_CABRAS;
        //jogadores[1] == JOGADOR_TIGRES;
        this.manager = manager;
    }

    public int entraNaPartida(int id) {
        if (jogadores[0]==0) { // id do jogador 0
            jogadores[0]=id;
            return JOGADOR_CABRAS;

        } else if(jogadores[1]==0) { // id do jogador 1
            jogadores[1]=id;
            return JOGADOR_TIGRES;

        } else return -1; // ja tem dois jogadores
    }

    public int saiDaPartida(int id) {
        int ret;
        if (jogadores[0]==id) { // id do jogador 0
            jogadores[0]=0;
            ret = 0;

        } else if(jogadores[1]==id) { // id do jogador 1
            jogadores[1]=0;
            ret = 0;

        } else ret = -1; // o id nao pertence a essa partida

        if (jogadores[0]==0 || jogadores[1]==0) manager.partidaEncerrou(this);
        return ret;
    }

    public char[][] getGrade() {
        return grade;
    }

    public boolean temJogador(int id) {
        if (jogadores[0]==id || jogadores[1]==id) {
            return true;
        }
        return false;
    }

    public int getJogadorRole(int id) {
        if (!temJogador(id)) return -1;
        if (jogadores[0]==id) return JOGADOR_CABRAS;
        else return JOGADOR_TIGRES;
    }

    public int ehMinhaVez(int id) {
        if (jogadores[0]!=0 && jogadores[1]!=0) {
            if (jogadores[0]==id) {
                return vez==0? 1: 0; // vez do 0?

            } else if (jogadores[1]==id) {
                return vez==1? 1: 0; // vez do 1?

            } else {
                return -1; // id nao esta na partida
            }
        }
        else return -2; // ainda nao ha 2 jogadores

    }

    public boolean addCabra(int x, int y) {
        if (cabras<MAX_CABRAS) {
            if (grade[x][y]==' ') {
                grade[x][y] = getCabraCharByNumber(cabras);
                cabras++;
                vez = vez==1?0:1;
                return true;
            } else {
                return false; // ja tem algo nesse lugar
            }
        } else {
            return false; // ja foram 20 cabras
        }
    }

    public boolean moveCabra(char cabra, int dir) {
        int dest_x=0, dest_y=0;
        if (cabras==MAX_CABRAS) {
            int atual_x = -1, atual_y = -1;
            if (isCabraOnGrade(cabra)) {
                for (int i=0; i<5; i++) {
                    for (int j=0; j<5; j++) {
                        if (grade[i][j]==cabra) {
                            atual_x = j;
                            atual_y = i;
                            break;
                        }
                    }
                }
                if (dir==0) dest_x +=1;
                if (dir==1) {dest_x +=1; dest_y +=1;}
                if (dir==2) dest_y +=1;
                if (dir==3) {dest_x -=1; dest_y +=1;}
                if (dir==4) dest_x -=1;
                if (dir==5) {dest_x -=1; dest_y -=1;}
                if (dir==6) dest_y -=1;
                if (dir==7) {dest_x +=1; dest_y -=1;}

                if (podeMoverCabraDePara(atual_x, atual_y, dest_x, dest_y)) {
                    grade[atual_y][atual_x]=' ';
                    grade[dest_y][dest_x] = cabra;
                    vez = vez==1?0:1;
                }
            } else {
                return false; // tentando mover cabra nao existente
            }
        } else {
            return false; // precisa primeiro posicionar todas cabras
        }
        return true;
    }

    private boolean podeMoverCabraDePara(int atual_x, int atual_y, int dest_x, int dest_y) {
        // TODO:
        return true;
    }

    private boolean isCabraOnGrade(char cabra) {
        for (int i=0; i<5; i++) {
            for (int j=0; j<5; j++) {
                if (grade[i][j]==cabra) return true;
            }
        }
        return false;
    }

    private char getCabraCharByNumber(int number) {
        switch (number) {
            case 0:  return 'a';
            case 1:  return 'b';
            case 2:  return 'c';
            case 3:  return 'd';
            case 4:  return 'e';
            case 5:  return 'f';
            case 6:  return 'g';
            case 7:  return 'h';
            case 8:  return 'i';
            case 9:  return 'j';
            case 10: return 'k';
            case 11: return 'l';
            case 12: return 'm';
            case 13: return 'n';
            case 14: return 'o';
            case 15: return 'p';
            case 16: return 'q';
            case 17: return 'r';
            case 18: return 's';
            case 19: return 't';
            case 20: return 'u';
        }
        return 'z';
    }

    private int getCabraNumberByChar(char c) {
        switch (c) {
            case 'a': return 0;
            case 'b': return 1;
            case 'c': return 2;
            case 'd': return 3;
            case 'e': return 4;
            case 'f': return 5;
            case 'g': return 6;
            case 'h': return 7;
            case 'i': return 8;
            case 'j': return 9;
            case 'k': return 10;
            case 'l': return 11;
            case 'm': return 12;
            case 'n': return 13;
            case 'o': return 14;
            case 'p': return 15;
            case 'q': return 16;
            case 'r': return 17;
            case 's': return 18;
            case 't': return 19;
            case 'u': return 20;
        }
        return 26;
    }

}
