package lucasfeijo;

import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Main {

    public static void main(String[] args) {

        try {
            Server obj = new Partidas();

            Registry registry = LocateRegistry.createRegistry(2000);
            registry.rebind("BhagaChall", obj);
            System.out.println("Server ready");
        }
        catch (Exception e)
        {
            System.out.println("BhagaChagall Server error: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
