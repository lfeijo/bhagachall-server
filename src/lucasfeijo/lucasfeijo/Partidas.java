package lucasfeijo;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Created by lucasfeijo
 */
public class Partidas extends UnicastRemoteObject implements Server, Partida.Manager {

    private ArrayList<Jogador> jogadores;
    public static final int maxJogadores = 50; // sempre par

    private ArrayList<Partida> partidas;

    public Partidas() throws RemoteException {
        jogadores = new ArrayList<>(maxJogadores);
        partidas = new ArrayList<>(maxJogadores/2);
        for (int i = 0; i<maxJogadores/2; i++) partidas.add(i, new Partida(this));
    }

    //region -- RMI interface --

    @Override
    public int registraJogador(String nome) throws RemoteException {
        if (jogadores.size()<maxJogadores) {
            if (getJogador(nome)==null) {
                Jogador j = new Jogador(nome);
                jogadores.add(j);
                return j.getId();
            } else {
                return -1; // ja cadastrado
            }
        } else return -2; // numero maximo
    }

    @Override
    public int encerraPartida(int id) throws RemoteException {
        Jogador j = getJogador(id);
        if (j!=null) {
            for (Partida p: partidas) {
                if (p.temJogador(id)) p.saiDaPartida(id);
            }
            jogadores.remove(j);
            return 0;
        } else return -1; // nao existe id
    }

    @Override
    public int temPartida(int id) throws RemoteException {
        Jogador jogador = getJogador(id);
        if (jogador==null) return -1; // jogador invalido

            // entrou ha mais de 20 segundos:
        if (jogador.getEntrou().before(new Date(System.currentTimeMillis()/1000-20))) {
            jogadores.remove(jogador);
            return -2;
        }

        for (Partida p: partidas) {
            if (p.temJogador(jogador.getId())) { // ja esta na partida
                return p.getJogadorRole(jogador.getId());
            }
            int v = p.entraNaPartida(id);
            if (v==-1) { // partida cheia
                continue;
            } else {
                return v; // tigres ou cabras
            }
        }
        return 0; // ainda todas cheias
    }

    @Override
    public int ehMinhaVez(int id) throws RemoteException {
        for (Partida p: partidas) {
            int v = p.ehMinhaVez(id);
            if (v!=-1) { // nao estou nessa partida
                return v;
            }
        }
        return -1;
    }

    @Override
    public String obtemGrade(int id) throws RemoteException {
        for (Partida p: partidas) {
            if (p.temJogador(id)) {
                char grade[][] = p.getGrade();
                String gradeS = "";
                for (int i=0; i<5; i++) {
                    for (int j=0; j<5; j++) {
                        if (grade[i][j]==' ')gradeS+='.';
                        else gradeS+=grade[i][j];
                        gradeS+=' ';
                    }
                    gradeS+='\n';
                }
                return gradeS;
            }
        }
        return null;
    }

    @Override
    public int moveTigre(int id, int tigre, int dir) throws RemoteException {
        return 0;
    }

    @Override
    public int posicionaCabra(int id, int x, int y) throws RemoteException {
        for (Partida p: partidas) {
            if (p.temJogador(id)) {
                if (p.ehMinhaVez(id)==1) {
                    if (p.getJogadorRole(id) == Partida.JOGADOR_CABRAS) {
                        return p.addCabra(x, y) ? 1 : 0; // ok ou movimento invalido
                    } else {
                        return -4; // animal incorreto
                    }
                } else {
                    return -3;
                }
            }
        }// else
        return -2;
    }

    @Override
    public int moveCabra(int id, char cabra, int dir) throws RemoteException {
        for (Partida p: partidas) {
            if (p.temJogador(id)) {
                if (p.ehMinhaVez(id)==1) {
                    if (p.getJogadorRole(id) == Partida.JOGADOR_CABRAS) {
                        return p.moveCabra(cabra, dir) ? 1 : 0; // ok ou movimento invalido
                    } else {
                        return -4; // animal incorreto
                    }
                } else {
                    return -3;
                }
            }
        }// else
        return -2;
    }

    @Override
    public String obtemOponente(int id) throws RemoteException {
        Jogador j = getJogador(id);
        if (j!=null) return j.getNome();
        else return null;
    }

    //endregion

    private Jogador getJogador(String nome) {
        for (Jogador j : jogadores) {
            if (j.getNome().equals(nome)) {
                return j;
            }
        }
        return null;
    }

    private Jogador getJogador(int id) {
        for (Jogador j : jogadores) {
            if (j.getId() == id) {
                return j;
            }
        }
        return null;
    }

    @Override
    public void partidaEncerrou(Partida partida) {
        for (Partida p : partidas) {
            if (p == partida) {

            }
        }
    }
}
