package lucasfeijo;

import java.util.Date;

/**
 * Created by lucasfeijo
 */
public class Jogador {

    public Jogador(String nome) {
        this.nome = nome;
        this.id = Math.abs(nome.hashCode());
        this.entrou = new Date();
        this.jogando = false;
    }

    private Date entrou;
    private int id;
    private String nome;
    private boolean jogando;

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Date getEntrou() {
        return entrou;
    }

    public boolean isJogando() {
        return jogando;
    }

    public void setJogando(boolean jogando) {
        this.jogando = jogando;
    }
}
